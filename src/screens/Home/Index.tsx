import React, {useState} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import NavigationBtn from '../../components/Buttons/NavigationBtn';
import PaginationBtn from '../../components/Buttons/PaginationBtn';
import ListMangas from '../../components/ListMangas/Index';

function Index() {

  //Constante para botones de paginacion
  const [page, setPage] = useState(1);

  return (
    <SafeAreaView>
      <ScrollView>
        <Text style={styles.titleText}>Bienvenido Terricola</Text>
        <ListMangas page={page} />
        <PaginationBtn page={page} setPage={setPage} />
        <View style={styles.containerBtns}>
          <NavigationBtn screenName={'CheckoutCart'} textBtn={'Pay Cart'} />
          <NavigationBtn 
            screenName={'Map'} 
            textBtn={'Tiendas'} 
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  box: {
    paddingHorizontal: 5,
    paddingVertical: 5, 
    width: 200,
    height: 300,
    padding: 5,
  },
  contenido: {
    flex: 0,
    flexWrap: 'wrap',
    alignItems: 'baseline',
    justifyContent: 'center',
    borderRadius: 5,
    marginVertical: 10,
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  titleText: {
    marginVertical: 10, 
    fontWeight: '800', 
    fontSize: 18,
    color: '#4b5563',
    textAlign: 'center',
  },
  containerBtns: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 20,
  },
});

export default Index;