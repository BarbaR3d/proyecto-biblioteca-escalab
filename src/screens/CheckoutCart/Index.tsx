import React, {useContext} from 'react';
import {
  Text, 
  SafeAreaView,
  View, 
  ScrollView, 
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import NavigationBtn from '../../components/Buttons/NavigationBtn';
import MangaItem from '../../components/MangaItem/Index';
import {CartContext} from '../../navigation/Index';
import PaymentBtn from '../../components/Buttons/PaymentBtn';

const Index = ({}) => {

  //Para agregar al carrito
  const {cart, setCart}: any = useContext(CartContext);

  return (
    <SafeAreaView>
      <ScrollView 
        contentContainerStyle={{
          justifyContent: 'center', 
          width: '100%'
        }}>
        {cart.map(manga => {
          return (
            <View key={manga?.id}>
              <TouchableOpacity 
                style={{
                  backgroundColor: 'black', 
                  width: 30,
                  position: 'absolute',
                  zIndex: 1,
                  top: 0,
                  right: 20,
                  borderRadius: 5,
                }}
                onPress={() => {
                  const newCart = cart.filter(mangaI => mangaI != manga);
                  setCart(newCart);
                }}>
                <Text
                  style={{color: 'white', fontSize: 25, textAlign: 'center'}}>
                  X
                </Text>
              </TouchableOpacity>
              <MangaItem manga={manga} />
            </View>
          );
        })}
        <NavigationBtn screenName={'Home'} textBtn={'Go to Home'} />
        <PaymentBtn cart={cart} setCart={setCart} />
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  containerBtns: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 20,
  },
  titleText: {
    marginVertical: 5, 
    marginHorizontal: 10,
    fontWeight: '800', 
    fontSize: 18,
    color: '#4b5563',
  },
  contentText: {
    color: '#404040',
  },
});

export default Index