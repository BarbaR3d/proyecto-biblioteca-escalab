import React from 'react';
import {View, StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

function Index() {
  return (
    <View style={styles.body}>
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: -33.438398,
            longitude: -70.6478546, 
            //Estos dos parametros determinan la extension del mapa
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }} 
        >
          <Marker coordinate={{
            latitude: -33.438398,
            longitude: -70.6478546,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
            }} />
          <Marker coordinate={{
            latitude: -33.4383867,
            longitude: -70.6511376,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
            }} />
          <Marker coordinate={{
              latitude: -33.4222959,
              longitude: -70.6123664,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
              }} />
        </MapView>
    </View>
  )
}

const styles = StyleSheet.create({
  body:{
    flex: 1,
    alignItems: 'center',
  },
  map:{
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});

export default Index;
