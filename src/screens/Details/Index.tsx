import {useRoute} from '@react-navigation/native';
import React, {useContext} from 'react';
import {
  Text, 
  SafeAreaView, 
  Image, 
  View, 
  ScrollView, 
  StyleSheet
} from 'react-native';
import NavigationBtn from '../../components/Buttons/NavigationBtn';
import AddOrDeleteBtn from '../../components/Buttons/AddOrDeleteBtn';
import {CartContext} from '../../navigation/Index';

const Index = ({}) => {

  const route = useRoute();
  //console.log(route);
  const manga = route?.params?.item;
  //Separar los elementos de generos
  let generos = manga.genres.join(', ');
  
  //Especificamos que contexto usaremos y que elementos
  const {cart, setCart}: any = useContext(CartContext);
  //console.log(cart, setCart);
  
  return (
    <SafeAreaView>
      <ScrollView>
        <View>
          <Image
            source={{uri: manga.coverImage.large}}
            style={{height: 400, width: 250, marginHorizontal: 70, borderRadius: 5,}} 
          />
          <Text style={styles.titleText}>
              Nombre: {
                <Text style={styles.contentText}>{manga.title.romaji}</Text>
              }
          </Text>
          <Text style={styles.titleText}>
            Generos: {
              <Text style={styles.contentText}>{generos}</Text>
            }
          </Text>
          <Text style={styles.titleText}>
            Estado: {
              "RELEASING" == manga.status ? 
                <Text style={{color: 'green'}}>{manga.status}</Text> 
                : 
                <Text style={{color: 'red'}}>{manga.status}</Text>
            }
          </Text>
          <Text style={styles.titleText}>
            Descripción: {
              <Text style={styles.contentText}>{manga.description}</Text>
            }
          </Text>
          <AddOrDeleteBtn setCart={setCart} cart={cart} manga={manga} />
          <NavigationBtn screenName={'CheckoutCart'} textBtn={'Pay Cart'} />
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  titleText: {
    marginVertical: 5, 
    marginHorizontal: 10,
    fontWeight: '800', 
    fontSize: 18,
    color: '#4b5563',
  },
  contentText: {
    color: '#404040',
  },
});

export default Index