import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../screens/Home/Index';
import Details from '../screens/Details/Index';
import CheckoutCart from '../screens/CheckoutCart/Index';
import Map from '../screens/Map/Index';

const Stack = createNativeStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home" 
        component={Home} 
        options={{title: 'Biblioteca'}} 
      />
      <Stack.Screen 
        name="Details" 
        component={Details} 
        options={{title: 'Detalles Manga'}}
      />
      <Stack.Screen 
        name="CheckoutCart" 
        component={CheckoutCart} 
        options={{title: 'Pagar Mangas'}}
      />
      <Stack.Screen 
        name="Map" 
        component={Map} 
        options={{title: 'Nuestras Tiendas'}} 
      />
    </Stack.Navigator>
  );
};

export default StackNavigator;
