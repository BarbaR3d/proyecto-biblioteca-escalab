import React, {createContext, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import StackNavigator from './StackNavigator';
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql
} from "@apollo/client";
import {enableLatestRenderer} from 'react-native-maps';

enableLatestRenderer();

//Context
export const CartContext = createContext([]);
//console.log(CartContext, 1);

const Index = () => {

  //Se define endpoint principal a la api
  const client = new ApolloClient({
    uri: 'https://graphql.anilist.co',
    cache: new InMemoryCache(),
  });

  const [cart, setCart] = useState([]);

  return (
    <ApolloProvider client={client}>
      <NavigationContainer>
        <CartContext.Provider value={{cart, setCart}}>
          <StackNavigator />
        </CartContext.Provider>
      </NavigationContainer>
    </ApolloProvider>
  );
};

export default Index;
