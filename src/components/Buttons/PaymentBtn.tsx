import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

const PaymentBtn = ({cart, setCart}) => {

  //console.log(cart);
  
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => {
          setCart([])
        }}
        style={styles.buttonAdd}>
        <Text style={styles.textBtn}>Pay Cart</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonAdd: {
    backgroundColor: '#10b981',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 5,
    margin: 5,
    marginHorizontal: 35,
  },
  textBtn: {
    color: '#eff6ff',
    fontSize: 18,
    fontWeight: '800',
    textAlign: 'center',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 20,
  },
});

export default PaymentBtn;