import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

const AddOrDeleteBtn = ({cart, setCart, manga}) => {

  //console.log(cart);
  //Funcion que retorna booleano
  const areInCart = cart.find(mangaI => mangaI.id == manga.id);
  //console.log(areInCart);

  
  
  return (
    <View style={styles.container}>
      {areInCart ? (
        <TouchableOpacity
          onPress={() => {
            const newCart = cart.filter(mangaI => mangaI.id != manga.id);
            setCart(newCart);
          }}
          style={styles.buttonDelete}>
          <Text style={styles.textBtn}>Delete Cart</Text>
        </TouchableOpacity>
      ) : null}
      <TouchableOpacity
        onPress={() => {
          if (manga) setCart([...cart, manga]); //Se guarda el carro anterior (...cart) y se agrega el item
        }}
        style={styles.buttonAdd}>
        <Text style={styles.textBtn}>Add Cart</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonAdd: {
    backgroundColor: '#10b981',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 5,
    margin: 5,
    marginHorizontal: 35,
  },
  buttonDelete: {
    backgroundColor: '#dc2626',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 5,
    margin: 5,
    marginHorizontal: 35,
  },
  textBtn: {
    color: '#eff6ff',
    fontSize: 18,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 20,
  },
});

export default AddOrDeleteBtn;