import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

const Button = ({screenName, textBtn}) => {
  //const [counter, setCounter] = useState(0);
  const navigation = useNavigation(); //Esto es un hoock

  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(screenName);
      }}
      style={styles.button}>
      <Text style={styles.textBtn}>{textBtn}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#0d9488',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 5,
    margin: 5,
    marginHorizontal: 35,
  },
  textBtn: {
    //marginVertical: 10, 
    //marginHorizontal: 50,
    fontWeight: '800', 
    color: '#eff6ff',
    fontSize: 18,
    textAlign: 'center',
  },
});

export default Button;