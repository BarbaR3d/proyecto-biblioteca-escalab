import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

const PaginationBtn = ({page, setPage}) => {

  const isPageOne = page > 1 ? false : true;
  
  return (
    <View style={styles.container}>
      {!isPageOne ? (
        <TouchableOpacity
          onPress={() => {
            if (page > 1) {
              setPage(page - 1);
            }
          }}
          style={styles.button}>
          <Text style={styles.textBtn}>Last Page</Text>
        </TouchableOpacity>
      ) : null}
      <TouchableOpacity
        onPress={() => {
          setPage(page + 1);
        }}
        style={styles.button}>
        <Text style={styles.textBtn}>Next Page</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#0d9488',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 5,
    margin: 5,
    marginHorizontal: 35,
  },
  textBtn: {
    color: '#eff6ff',
    fontSize: 18,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 20,
  },
});

export default PaginationBtn;