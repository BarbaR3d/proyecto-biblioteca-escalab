import React from 'react';
import {
  Image, 
  Text, 
  TouchableOpacity, 
  View, 
  Dimensions, 
  StyleSheet
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const width = Dimensions.get('window').width; //Calcular ancho pantalla

const Index = ({manga}) => {

  const navigation: any = useNavigation();
  //console.log(manga);
  //Separar los elementos de generos
  let generos = manga.genres.join(', ');
  
  return (
    <TouchableOpacity 
      onPress={() => {
        navigation.navigate('Details', {
          item: manga,
        });
      }}
      style={styles.box}
    >
      <View key={manga.id} style={styles.contenido}>
        <Image 
          source={{uri: manga?.coverImage.large}}
          style={{height: 300, width: width * 0.44, borderRadius: 5}}
        />
        <Text style={styles.titleText}>
          {manga.title.romaji}
        </Text>
        <Text style={styles.titleText}>
          Estado: {
                "RELEASING" == manga.status ? 
                  <Text style={{color: 'green'}}>{manga.status}</Text> 
                  : 
                  <Text style={{color: 'red'}}>{manga.status}</Text>
              }
        </Text>
        <Text style={styles.titleText}>
          Generos: {
            <Text style={styles.contentText}>{generos}</Text>
          }
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  box: {
    //flexWrap: 'wrap',
    //flexDirection: 'column',
    //alignContent: 'center',
    //justifyContent: 'space-between',
    paddingHorizontal: 5,
    paddingVertical: 5, 
    //margin: 10,
    //maxWidth: '50%',
    width: 200,
    height: 300,
    //alignSelf: 'center',
    padding: 5,
  },
  contenido: {
    flex: 0,
    flexWrap: 'wrap',
    alignItems: 'baseline',
    justifyContent: 'center',
    //backgroundColor: 'pink',
    borderRadius: 5,
    marginVertical: 10,
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  titleText: {
    marginVertical: 5, 
    marginHorizontal: 10,
    fontWeight: '800', 
    fontSize: 18,
    color: '#4b5563',
  },
});

export default Index;