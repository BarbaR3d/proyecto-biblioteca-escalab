import React from 'react';
import {
  ActivityIndicator, 
  ScrollView, 
  StyleSheet, 
  Text, 
  View
} from 'react-native';
import MangaItem from '../../components/MangaItem/Index';
import {useQuery} from '@apollo/client';
import {MANGA_QUERY} from './Querys';

function Index({page}) {

  const {data, loading, error} = useQuery(MANGA_QUERY, {
    variables: {page: page, perPage: 10},
  });

  //Constante para en caso de no funcionar la api, renderizar un "producto" y no se rompa la app
  const apiData = data?.Page?.media 
    ? data?.Page?.media 
    : [
        {
          "id": 30002,
          "title": {
            "romaji": "Berserk",
            "english": "Berserk"
          },
          "type": "MANGA",
          "genres": [
            "Action",
            "Adventure",
            "Drama",
            "Fantasy",
            "Horror",
            "Psychological"
          ],
          "status": "RELEASING",
          "description": "His name is Guts, the Black Swordsman, a feared warrior spoken of only in whispers. Bearer of a gigantic sword, an iron hand, and the scars of countless battles and tortures, his flesh is also indelibly marked with The Brand, an unholy symbol that draws the forces of darkness to him and dooms him as their sacrifice. But Guts won't take his fate lying down; he'll cut a crimson swath of carnage through the ranks of the damned—and anyone else foolish enough to oppose him! Accompanied by Puck the Elf, more an annoyance than a companion, Guts relentlessly follows a dark, bloodstained path that leads only to death...or vengeance.<br>\n<br>\n(Source: Dark Horse)<br>\n<br>\n<i>Notes:<br>\n- Volumes 1-5 contain the 16 prequel chapters 0A - 0P.<br>\n- <a href=\"https://anilist.co/manga/95821/\">Chapter 83</a> was omitted from Volume 13 due to the author’s request.<br>\n- Volume 14 includes “<a href=\"https://anilist.co/manga/36408/\">Berserk: The Prototype</a>”.<br>\n- Due to the author's passing, starting from Chapter 365, the manga is illustrated by Studio Gaga (Miura's assistants) and supervised by Kouji Mori (a close friend of Miura's).</i>",
          "coverImage": {
            "extraLarge": "https://s4.anilist.co/file/anilistcdn/media/manga/cover/large/bx30002-7EzO7o21jzeF.jpg",
            "large": "https://s4.anilist.co/file/anilistcdn/media/manga/cover/medium/bx30002-7EzO7o21jzeF.jpg",
            "medium": "https://s4.anilist.co/file/anilistcdn/media/manga/cover/small/bx30002-7EzO7o21jzeF.jpg",
            "color": "#e4a143"
          }
        }
      ];
  

  if (loading) { //Al cargar info se ve loading
    return <ActivityIndicator size={'large'} style={{padding: 50}} />;
  }

  if (error) return <Text>El servidor ha fallado</Text>;

  return (
    <ScrollView>
      <View style={styles.container}>
        {apiData.map(manga => {
          return <MangaItem manga={manga} key={manga?.id} />;
        })}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '92%',
    height: '41%',
    marginHorizontal: 13,
    marginVertical: 5,
    //justifyContent: 'space-between',
    //alignContent: 'space-around',
    //paddingBottom: 20,
    //paddingVertical: 20,
    //maxWidth: '90%',
    //maxHeight: '0.5%',
  },
});

export default Index;