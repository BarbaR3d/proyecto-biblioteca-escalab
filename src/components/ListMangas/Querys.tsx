import {useQuery, gql} from '@apollo/client';

//Query GraphQL
export const MANGA_QUERY = gql`
  query ($id: Int, $page: Int, $perPage: Int, $search: String) {
    Page(page: $page, perPage: $perPage) {
      media(id: $id, search: $search, type: MANGA, sort: FAVOURITES_DESC) {
        id
        title {
          romaji
          english
        }
        type
        genres
        status
        description
        coverImage {
          extraLarge
          large
          medium
          color
        }
      }
    }
  }
`;

//ejemplo query graphQL
/*const query = `
    query ($id: Int, $page: Int, $perPage: Int, $search: String) {
      Page (page: $page, perPage: $perPage) {
        pageInfo {
          total
          currentPage
          perPage
        }
        media (id: $id, search: $search, type: MANGA, sort: FAVOURITES_DESC) {
          id
          title {
            romaji
            english
          }
          type
          genres
          status
          description
          coverImage {
            extraLarge
            large
            medium
            color
          }
        }
      }
    }
  `;*/