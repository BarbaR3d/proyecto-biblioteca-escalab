import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const Product = ({manga}) => {

  const navigation = useNavigation();

  return (
    <>
      <TouchableOpacity 
        onPress={() => {
          navigation.navigate('Details', {
            item: manga;
          });
        }}
        style={{padding: 20, margin: 10, backgroundColor: 'lightBlue'}}
      >
        <Text style={{marginVertical: 5}}>Producto Terricola {manga.name}</Text>
        <View key={manga.id} style={{alignItems: 'center', padding: 10}}>
          <Text style={{marginVertical: 5, fontWeight: 800, fontSize: 18}}>{manga.title.romaji}</Text>
          <Image 
            source={{uri: manga?.coverImage.medium}}
            style={{height: 200, width: 300, borderRadius: 5}}
          />
        </View>
      </TouchableOpacity>
    </>
  );
};

export default Product;